#!/bin/bash
rm federabot/bot.log federabot/err.log haiku/bot.log haiku/err.log prevert/bot.log prevert/err.log eclipses/bot.log eclipses/err.log

python3 federabot/federabot.py federabot/ >federabot/bot.log 2>federabot/err.log &
python3 haiku/haikubot.py haiku/ >haiku/bot.log 2>haiku/err.log &
python prevert/prevertbot.py prevert/ >prevert/bot.log 2>prevert/err.log &
python3 eclipses/eclipsebot.py eclipses/ >eclipses/bot.log 2> eclipses/err.log &
python3 dicodigital/dicobot.py dicodigital/ >dicodigital/bot.log 2>dicodigital/err.log &

disown -a
