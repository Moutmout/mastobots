# coding=utf-8

from mastodon import Mastodon
from time import *
from random import *
import sys

sys.path.insert(0, '..')
from botinit import get_api

current_directory = sys.argv[1]

api, MASTODON_USER_ID = get_api(current_directory + "keys.txt")

texts = [i.strip() for i in open(current_directory + "phrases.txt").readlines()]
answers = [i.strip() for i in open(current_directory + "answers.txt").readlines()]

account_handles = [i.strip() for i in open(current_directory + "handles.txt").readlines()]

accounts = []
for e in account_handles:
    accounts.extend(api.account_search(e))

dernier_toot = api.account_statuses(accounts[0])[0]
dernier_notif = api.notifications()[0]

while True:
    print("Waking up")
    # Answer a toot at random
    notifs = []
    for e in accounts:
        notifs.extend(api.account_statuses(e, since_id = dernier_toot))
        # dernier_toot non inclus dans notifs

    try:
        notif = choice(notifs)
        dernier_toot = notifs[0]
    except IndexError:
        notif = dernier_toot
    toot_text = choice(texts)
    print("Telling", notif.account.username, toot_text)
    api.status_post(
        toot_text,
        in_reply_to_id = notif['id'],
        visibility = 'public'
    )

    # Reply to a mention
    latest_mentions = [e for e in api.notifications(since_id=dernier_notif)
                       if e.type=='mention']
    try:
        mention = choice([e for e in latest_mentions])
    except IndexError:
        print("No mentions")
    else:
        toot_text = "@" + str(mention.account.acct) + " " + choice(answers)
        print("Answering", mention.account.username)
        api.status_post(
            toot_text,
            in_reply_to_id = mention.status.id,
            visibility = mention.status.visibility
        )
        dernier_notif = mention

    # Fav all replies
    latest_replies = [e for e in latest_mentions
                      if e.status.in_reply_to_account_id == MASTODON_USER_ID
    ]
    for mention in latest_replies:
        print("Favouriting", mention.account.username)
        api.status_favourite(mention.status)

    # Done
    print("Back to sleep")
    sleep(3600)
