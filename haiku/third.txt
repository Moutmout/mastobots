soir d'automne
des soirées d’automne
mais un tel parfum
l’alouette chante
le cri du faisan
le bruit de l’eau
papillon qui dort
sur le saule
fleur de la camélia
ah ! quelle soirée
fleurs de cerisiers
se sont raccourcies
les larmes aux yeux
est bien admirable
toujours occupé
le chant des cigales
dans l’ombre des arbres
oiseau dans les nuages
donc cet homme au juste ?
ce soir d'automne
ne l’oublie jamais
jamais mon amie
feuille de paulownia
première bruine
une boule de neige
le bruit du vent
kotatsu mobile
contre ce poteau
d’un petit manteau
m’a réveillé
les champs désolés
du même vert
ma cape de voyageur
la fumée du thé
de la colline rocheuse
en y ajoutant celle du faisan
les yeux des poissons en larmes
parfume ses ailes
au parfum des pruniers
fait vibrer le silence
une pieuvre rêve
lacets pour sandales
l'emprunte encore
le vent d'automne
le maître et moi
comme ils ont froid !
s'éclaircit
difficile à dire
une femme lit une lettre
la lune voilée
sont devenus fruits
a éclos
ce soir
la pivoine
deux ou trois pétales
le bruit noir
nagent dans les gués
une fleur de chèvrefeuille
éclate de rire
le chant des insectes
j’ai l’impression de le voler
le moine hésite
le chant des grenouilles
l’eau et le ciel
puis poursuit son chemin
dans les bambous
le bosquet en été
ce matin de rosée
matin d’automne
matin d’automne
tempête d’automne
réclusion hivernale
le brasero
nuit froide
ce matin de neige
réjouissante
semble-t-il
de cette nuit
est une affaire de demain
qui se termine
sur le pont de bois
l’eau et le ciel
halo de la lune
paille et parapluie
les eaux de printemps
comme il devient triste
la vie d’autrefois
toute une journée !
ah ! le papillon
le saule pleureur
branches de prunier
est perdu de vue
la blancheur du riz
sa danse ondulante
à ce même endroit
prunier sur la rive
son bec minuscule
de ce côté-ci
cerisier pleureur
le mont Yoshino
ah ! quel passe-temps
le soleil à l’ouest
une robe à fleurs
le printemps s’en va
fleurs de cerisier
pleurant le printemps
en me promenant
la voix de la cloche
perles de rosée
rendue redoutable
la pivoine blanche
sandales en main
tombant des bambous
ce matin d’automne
l’automne à Suma
contempler la lune
tempête d’automne
le cerf sous la pluie
ah ! soleil couchant
à mon bâton d’encre
la fin de l’automne
ses érables rouges
des feuilles d’érable
l’herbe des pampas
de ma femme morte
les feuilles d’automnes
terriblement froid
nuit de gel !
au milieu du pont
le gardien du temple
le bosquet d’hiver
que le ciel est haut
quelle détresse !
parcourt le koto
ah ! neige fondue
vient le premier gel
contre les rochers
ah ! lande pelée
emportées par l’eau
Ah ! le brasero
n’aura pas de fin
du mont Yoshino
du vieux puits
puis revient
je commence une longue lettre
ayant renoncé à tout
sont tordus
où se réunissent les moineaux
dans le vent
une fumée
je fais la grasse matinée
dans un coin ensoleillé
poitrine
sur le quai où je passe ce soir
s’amenuise
de tout leur coeur
sous le ciel immense
ne se rendent compte de rien
en pleine floraison
une silhouette
le bruit de leurs ailes
violettes en fleur
bien mieux qu’un rêve
humide et glacé
le moine Ryôkan
pas le moindre bruit
crépuscule d’automne
le riz du matin
y prêtent attention
bambous sous la neige
m’a enivré
par le vent d’automne
la nuit entière
les fleurs du prunier
matinée de neige
la pluie automnale
au petit matin
bambou de l’année
des pivoines blanches
assis dans le froid
les feuilles d’automne
l’herbe du jardin
le chant des fauvettes
un panier de fleurs
la fraîcheur du soir !
la danse de Bon
pour mon éventail
veillant jusqu’à l’aube
des grenouilles vertes
averses de mai
la lune par la fenêtre
matin de printemps
le mont Yahiko
jusqu’à la rosée
pourtant familier
tapant sur mon bol
ma gourde fêlée
j’y pense aujourd’hui
ôter mon bonnet
le chant des fauvettes
et l’homme vieillit
la bruine du soir
comme on s’attendrit
le chant des fauvettes
de deux ou trois pieds
un aigle qui fixe sa proie
au sortir du bain
le vent du printemps
le vent de l’automne
les coquelicots
la pluie de printemps
apparaissent partout
de mon jardin
la robe des Tang
le vent froid d’automne
des grenouilles vertes
retentit du bois
loin de ma cabane
où poussent les prêles
ma mélancolie ?
des grenouilles vertes
la fleur de lotus
le chant des insectes
les vagues comme oreiller
fleurs de cerisier
aiguilles de pin
que gourde ou que loche
l’oiseau du lotus
oies cendrées du soir
son chant est le même
sans le moindre but
les petits moineaux
ne l’est plus
n’avait aucun but
comme l’air est frais
comme je suis seul !
dans la grande salle
les amants se quittent
grenouille coassante
tombée en silence
moineau sans parents
a été touchée
de vaines parlottes
est source de larmes
matin de printemps
le son d’une cloche
combien de printemps
n’ayant autre à faire
tombe, tombe la pluie
la cime des nuages
des lucioles en vol
telle est ma demeure
ces jours de plaisir
l’eau de cuisson du riz
là-bas sous les saules
fumées du matin
petite sauterelle
aboiements de chiens
les chants des planteurs
lors de mes adieux
dans le frais du soir
était un adieu
colimaçon
peut-être l’ultime
par puces et moustiques
pleure une grenouille
la rosée des herbes
coasse une grenouille
petit papillon
et moi resté seul
en prenant leur vol
se hâtent d’y rentrer
le mont Fuji
il y a bien tout
heureux seul
l’infortuné ?
d’être du voyage
les pluies de mai
au cœur du bosquet
on la voit enfin !
une araignée pleine
la couleur de l’eau
la longue journée
rêve de singe
errent mes pensées
vivants toi et moi
les nuits sont si brèves
cigales d’automne
le petit canard
oui, sans doute et pourtant
qu'elle aurait tant aimé arracher
d'un insecte d'un nom inconnu
où il flottait hier
j'en ajoute encore
sur le dos d'une chenille velue
froide nuit automnale
voilà le souvenir d'Edo
fumée de la torréfaction du thé
les yeux des poissons portent des larmes
la montagne et le jardin
embaume la neige
que les pierres de la colline rocheuse
au lac des grèbes
dans cette tempête
fleurs de sarrasin
une seule goutte de rosée
dans de vieilles algues
grimpe sur un pin
pluie de juin
jeunes feuillages
a rompu la tige de mauve
de grosses vagues
un petit temple
étouffée de crachats
seuls restent dans les herbes
un peu de rouge
sur les envers des chapeaux en carex
maison de chrysanthèmes
tombent de ma main
naît là
s'étendent dans toutes les directions
je n'y trouve rien
"Jaune."
et elle est repartie
sous le soleil brûlant
pays de Shinano
se mettent à remuer
l'ombre des cèdres
devant les cerisiers fleuris
bien trop de fleurs
Ah ! cette violette
du printemps
tombèrent des fleurs de camélia
aux branches
du fond de la pluie
te feront dessus
et le saule
la grenouille !
vont avec la maisonnette
m'amusais à dessiner
était sans âme
cerf-volant
savates à la main
le premier ciel de l'année
ont disparu de la terre
de ce monde
le printemps s'en va
dans le filet de la brume
rien de plus
un arbre d'hiver
monté là-haut ?
hésite
pèsent sur moi
de la rumeur du pin
a disparu
froide nuit automnale
le lac Suwa
vêtements de chanvre
ce matin d'automne
descend la montagne
je m'assieds
soir d'automne !
se reposent
que le couchant d'automne
le chat la retient de sa patte
se distord
une nuit d'étoiles
crépuscule du soir
une baie rouge
j'ignore où tu vas
crépuscule du soir
les racines du camphrier
crissent sous les pas
un coq chante
dans l'isolement de l'hiver
la pluie dans la montagne
sur la lande désséchée
sur la gelée blanche du jardin
