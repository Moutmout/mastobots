# coding=utf-8

from datetime import datetime
from html.parser import HTMLParser
import urllib.request

startline = 278
endline = 536

#webpage = "https://eclipse.gsfc.nasa.gov/LEdecade/LEdecade2011.html"
#websource = str(urllib.request.urlopen(webpage).read())
webpage = "https://eclipse.gsfc.nasa.gov/LEcat5/LE2001-2100.html"

months = {'Jan' : 1,
          'Feb' : 2,
          'Mar' : 3,
          'Apr' : 4,
          'May' : 5,
          'Jun' : 6,
          'Jul' : 7,
          'Aug' : 8,
          'Sep' : 9,
          'Oct' : 10,
          'Nov' : 11,
          'Dec' : 12}

eclipse_types = {'N' : 'pénombrale',
                 'P' : 'partielle',
                 'T' : 'totale'}

class MyHTMLParser(HTMLParser):
    def __init__(self, *args, **kwargs):
        super(MyHTMLParser, self).__init__(*args, **kwargs)
        self.accu = []
        
    def handle_data(self, data):
        self.accu += (data.split())

def get_next():
    parser = MyHTMLParser()
    websource = urllib.request.urlopen(webpage).readlines()[startline:endline]

    present = datetime.now()
    date = present
    while date<=present:
        parser.accu = []
        parser.feed(str(websource.pop(0)))
        try:
            date = datetime(int(parser.accu[2]), months[parser.accu[3]], int(parser.accu[4]))
        except:
            pass
    return date, eclipse_types[parser.accu[9][0]]
    
    
def toot(edate, etype):
    res = "La prochaine éclipse lunaire aura lieu le {}/{}/{}. Ce sera une éclipse {}".format(edate.day, edate.month,edate.year, etype)
    return res


def main():
    print(toot(*get_next()))

if __name__ == "__main__":
    main()
